'use strict';
import React from 'react';
import SwitchNavigatorScreen from './src/config'
export default class App extends React.Component {
  render() {
    return(
      <React.Fragment>  
        <SwitchNavigatorScreen />
      </React.Fragment>
    );
  }
};
