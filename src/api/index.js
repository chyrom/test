import AsyncStorage from '@react-native-community/async-storage'
import moment from 'moment';

let _baseUrl = () => 'http://bvm-admin-dev.codingate.com';
let _authorize = null;
let _authorizeExpire = 0;
let _auth = null;
let _authExpire = 0;

/**
 * Convert query parameters to string
 * Ex: { id: 1, username: "john" } => 'id=1&username=john'
 * @param {object} params { key: value, ... }
 **/

function convertQueryParametersToString(params) {
    const ret = [];
    if (params) {
        for (let d in params) {
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(params[d]));
        }
    }
    return ret.join('&');
}

function getFullApiUrl(url) {
    console.log("get full url: ", url);
    return `${_baseUrl()}/api/${url}`;
}

function logAuth() {
    console.log(`Authorize: ${_authorize}, Authorize Expire: ${_authorizeExpire}, Auth: ${_auth}, Auth Expire: ${_authExpire}`);
}

function isAuthOk() {
    return _auth && moment().isBefore(moment.unix(_authExpire)) ? true : false
}

function isSuccessResponse(res) {
    return res && res.success ? true : false
}

function reqAuthorize() {
    return fetch(getFullApiUrl('authorize'), {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "device_id": "123",
            "device_name": "nexus 5",
            "device_info": "nexus 5 desc",
            "device_os": "android",
        }),
    }).then(res => res.json()).then(res => {
        _authorize = res.data.token;
        _authorizeExpire = res.data.expired_date;
        console.log('Authorize result: ', res);
        return AsyncStorage.multiSet([['@authorize', _authorize], ['@authorizeExpire', _authorizeExpire.toString()]]).then(() => {
            return res;
        });
    })
}

function api({ url, method, headers, data, param }) {
    if (_authorize && moment().isBefore(moment.unix(_authorizeExpire))) {
        console.log("if true run")
        return fetch(getFullApiUrl(`${url}?${convertQueryParametersToString(param)}`), {
            method,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorize: _authorize,
                ...headers
            },
            body: JSON.stringify(data),
        }).then(res => res.json());
    } else {
        return reqAuthorize().then(() => {
            return fetch(getFullApiUrl(`${url}?${convertQueryParametersToString(param)}`), {
                method,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorize: _authorize,
                    ...headers
                },
                body: JSON.stringify(data),
            }).then(res => res.json())
        });
    }
}

function apiAuth({ url, method, headers, data, param }) {
    return api({ url, method, headers: { Auth: _auth, ...headers }, data, param })
}

function apiPost({ url, headers, data, param }) {
    return api({ url, method: "POST", headers, data, param })
}

function apiGet({ url, headers, param }) {
    return api({ url, method: "GET", headers, param })
}

function apiAuthPost({ url, headers, data, param }) {
    return apiAuth({ url, method: "POST", headers, data, param })
}

function apiAuthGet({ url, headers, param }) {
    return apiAuth({ url, method: "GET", headers, param })
}

function clear() {
    return AsyncStorage.multiRemove(['@authorize', '@authorizeExpire', '@auth', '@authExpire']).then(() => {
        _authorize = null;
        _authorizeExpire = 0;
        _auth = null;
        _authExpire = 0;
    });
}

function load() {
    return AsyncStorage.multiGet(['@authorize', '@authorizeExpire', '@auth', '@authExpire']).then(res => {
        _authorize = res[0][1];
        _authorizeExpire = res[1][1] ? parseInt(res[1][1]) : 0;
        _auth = res[2][1];
        _authExpire = res[3][1] ? parseInt(res[3][1]) : 0;
    })
}

function reqLogin({email, password}){
    return apiPost({
        url: 'customer/auth/login',
        data: {
            email: email,
            password: password
        }
    })
}

const Api = {
    isAuthOk,
    isSuccessResponse,
    logAuth,
    load,
    clear,
    reqLogin
}

export default Api;
