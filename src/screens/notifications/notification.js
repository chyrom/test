import React, {Component} from 'react';
import {Platform, StyleSheet, Text,SafeAreaView,View,Image ,TouchableOpacity,ScrollView,FlatList, ImageBackground} from 'react-native';

DATA = [
  { "icon":require("../../asset/icon/notification/confirm.png"), "title":"Confirm booking PO","info":"Lorem Ipsum is simply dummy texxt of printing and typesetting industry. lorem Ipsum has been industry's standard dummy text.","date":"April 12,2019 at 8.21 AM"},
  { "icon":require("../../asset/icon/notification/cancel.png"), "title":"Cancel booking","info":"Lorem Ipsum is simply dummy texxt of printing and typesetting industry. lorem Ipsum has been industry's standard dummy text.","date":"April 12,2019 at 8.21 AM"},
  { "icon":require("../../asset/icon/notification/delivery.png"), "title":"Delivery Start","info":"Lorem Ipsum is simply dummy texxt of printing and typesetting industry. lorem Ipsum has been industry's standard dummy text.","date":"April 12,2019 at 8.21 AM"},
  { "icon":require("../../asset/icon/notification/notice.png"), "title":"Notice truck","info":"Delivery notice truck is approaching customer's place in 30km or 30mn","date":"April 12,2019 at 8.21 AM"},
]
export default class Notification extends Component {
    render() {
      const { navigation } = this.props;
      return (
        <View style={{flex: 1}}>
          <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
            <ScrollView style={{width: '100%',height:'100%'}}>
              <SafeAreaView>
                <View style={{marginBottom: 100, paddingLeft: 10, paddingRight: 10}}>
                  <View style={{marginTop: Platform.OS === "ios"? 30 : 40, alignItems: 'center'}}>
                    <Text style={styles.title}>Notification</Text>
                  </View>
                  <View style={{marginTop: Platform.OS === "ios" ? 20 : 20}}>
                    <FlatList 
                      data={DATA}
                      renderItem={({ item }) =>
                      <TouchableOpacity  onPress={() => this.props.navigation.navigate('NotificationDetailScreen', {icon:item.icon,title:item.title,info:item.info,date:item.date})} 
                        style={{marginTop:15,backgroundColor:'#ffffff',borderRadius:18,paddingBottom:18,paddingTop: 24,paddingRight: 33,paddingLeft: 35,width:'100%',height:170}}>
                        <View style={{flex:1,flexDirection:'row',alignItems:'flex-start',justifyContent:'flex-start'}}>
                          <Image 
                          style={{width:20,height:20, resizeMode:'contain'}}
                          source={item.icon}/>
                      <Text style={{color:'#000000',marginLeft:7,fontSize:15}}>{item.title}</Text>
                        </View>
                        <View style={{flex:3,marginTop: 9}}> 
                          <Text style={{fontSize:15,color: '#6A6A6A'}}>{item.info}</Text>
                        </View>
                        <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end',marginTop: 28}}>
                          <Text  style={{fontSize:13,alignSelf:'flex-end',color: '#5D5D5D'}}>{item.date}</Text>
                        </View>
                      </TouchableOpacity>}
                    />

                  </View>
                </View>
              </SafeAreaView>
            </ScrollView>
          </ImageBackground>
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%',  
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  }
})