import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,SafeAreaView,ImageBackground, Image,TouchableOpacity} from 'react-native';

export default class NotificationDetail extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
          <SafeAreaView>
            <View style={{paddingLeft: 10, paddingRight: 10}}>  
              <View style={{marginTop: Platform.OS === "ios" ? 20 : 40, width: "100%", alignItems: 'center',height: "5%", flexDirection: 'row'}}>
                <TouchableOpacity 
                style={{width: '10%', alignItems: 'center', height: '100%', justifyContent: 'center'}}
                onPress={() => this.props.navigation.goBack()}> 
                <Image source={require('../../asset/icon/profile/backbtn.png')} 
                style={{width: 10, height: 14,resizeMode: 'stretch'}}/>
                </TouchableOpacity>                
                <View style={{height: '100%', width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={styles.title}>Notification</Text>
                </View>
              </View>
              <View style={{height: "95%",width: "100%",marginTop: Platform.OS === "ios" ? 20 : 20, backgroundColor: "#ffffff",borderTopLeftRadius: 22,borderTopRightRadius: 22}}>
                <View style={{width: "100%",height: 150,paddingLeft: 37,paddingRight: 18, marginTop: 24}}>
                  <View style={{flex:1,flexDirection:'row',alignItems:'flex-start',justifyContent:'flex-start'}}>
                    <Image  source={navigation.getParam('icon')}
                    style={{width: 19, height: 19, resizeMode:'contain'}}/>
                    <Text style={{marginLeft:7,color: '#000000', fontSize:15}}>{navigation.getParam('title')}</Text>
                  </View>
                  <View style={{flex:3, marginTop: 6}}>
                    <Text style={{fontSize:15,color: '#6A6A6A'}}>{navigation.getParam('info')}</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end'}}>
                    <Text style={{fontSize:13,alignSelf:'flex-end',color: '#5D5D5D'}}>{navigation.getParam('date')}</Text>
                  </View>
                </View>
              </View> 
            </View> 
          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%',  
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  }
})