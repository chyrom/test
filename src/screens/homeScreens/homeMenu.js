import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,SafeAreaView, ScrollView,TouchableOpacity,TextInput,ImageBackground,Image} from 'react-native';

export default class HomeMenu extends Component {

  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     isDelivery: true,
  //     isPending: false,
  //     isComplete: false
  //   };
  // }
  render() {
    return (
      <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
        <ScrollView style={{width: '100%', height: '100%'}}>
        <SafeAreaView>
          <View style={{width: '100%',height: '100%', paddingLeft: 10, paddingRight: 10, marginBottom: 100}}> 
            <View style={{ marginTop: Platform.OS === "ios"? 20 : 20, flex: 1}}>
              <View style={{flexDirection:"row",alignSelf:'center'}}>
                <Text style={{color:'green',fontWeight:'bold',fontSize:20, alignSelf:'center'}}>bvm</Text>
                <Text style={{color:'#000000',fontSize:20,alignSelf:'center',marginLeft:8}}>pretroleom</Text>
              </View>
              <View style={{flex: 1, marginTop: 25}}>
                <View style={{width: '100%', height: 400}}>
                  <View style={{width: '100%', height: '48%', flexDirection: 'row', justifyContent: 'space-between', marginBottom: '2%'}}>
                    <View style={styles.containeritem}> 
                      <View style={{flex: 1.5}}>
                        <Image source={require('../../asset/icon/home/pending.png')} style={styles.icons}/>
                      </View>
                      <View style={{flex: 1, justifyContent: 'flex-end'}}>
                        <Text style={styles.txt}>Pending.Doc</Text>
                        <Text style={styles.itemtxt}>13 items</Text>
                      </View>
                    </View>
                    <View style={styles.containeritem}> 
                      <View style={{flex: 1.5}}>
                        <Image source={require('../../asset/icon/home/complete.png')} style={styles.icons}/>
                      </View>
                      <View style={{flex: 1, justifyContent: 'flex-end'}}>
                        <Text style={styles.txt}>Complete</Text>
                        <Text style={styles.itemtxt}>13 items</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{width: '100%', height: '48%', flexDirection: 'row', justifyContent: 'space-between', marginTop: '2%'}}>
                    <View style={styles.containeritem}> 
                      <View style={{flex: 1.5}}>
                        <Image source={require('../../asset/icon/home/comingup.png')} style={styles.icons}/>
                      </View>
                      <View style={{flex: 1, justifyContent: 'flex-end'}}>
                        <Text style={styles.txt}>Coming Up</Text>
                        <Text style={styles.itemtxt}>13 items</Text>
                      </View>
                    </View>
                    <View style={styles.containeritem}> 
                      <View style={{flex: 1.5}}>
                        <Image source={require('../../asset/icon/home/delivery.png')} style={styles.icons}/>
                      </View>
                      <View style={{flex: 1, justifyContent: 'flex-end'}}>
                        <Text style={styles.txt}>Delivery</Text>
                        <Text style={styles.itemtxt}>13 items</Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.backgroundImgContainer}>
                  <ImageBackground source={require('../../asset/icon/home/background.png')} style={styles.background}>
                    <View style={{flex: 1, padding: 15}}>
                      <Text style={styles.txt}>Your Delivery History</Text>
                      <Text style={{marginTop: 10}}>32 Delivery</Text>
                    </View>
                    <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', flexDirection: 'row', padding: 15}}>
                      <Image source={require('../../asset/icon/home/red.png')} style={{width: 26, height: 26, marginRight: 9, resizeMode: 'stretch'}}/>
                      <Image source={require('../../asset/icon/home/yellow.png')} style={{width: 26, height: 26, marginRight: 9, resizeMode: 'stretch'}}/>
                      <Image source={require('../../asset/icon/home/green.png')} style={{width: 26, height: 26, resizeMode: 'stretch'}}/>
                    </View>
                  </ImageBackground>
                </View>
                <View style={styles.backgroundImgContainer}>
                  <ImageBackground source={require('../../asset/icon/home/background.png')} style={styles.background}>
                    <View style={{flex: 1, padding: 15}}>
                      <Text style={styles.txt}>Your Delivery History</Text>
                      <Text style={{marginTop: 10}}>32 Delivery</Text>
                    </View>
                  </ImageBackground>
                </View>
              </View>
            </View>
          </View>
        </SafeAreaView>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%'
  },
  cameraContainer:{
    width:40,
    height:40,
    borderRadius:20,
    position:'absolute',
    bottom:0,
    right:0
  },
  txt: {
    color: '#000000',
    fontSize: 18,
  },
  icons: {
    width: 40,
    height: 40,
    resizeMode: 'stretch'
  },
  itemtxt: {
    marginTop: 10,
    fontSize: 15,
    color: '#6A6A6A'
  },
  containeritem: {
    height: '100%', 
    width: '48%', 
    borderRadius: 18, 
    backgroundColor: '#faf7ef', 
    padding: 16
  },
  backgroundImgContainer: {
    width: '100%',
    height: Platform.Os==='ios'?140:145,
    borderRadius: Platform.Os==='ios'?30:16,
    marginTop: 15
  },
  background: {
    width: '100%',
    height: '100%',
    borderRadius: Platform.Os==='ios'?30:16,
    
  },
  
})