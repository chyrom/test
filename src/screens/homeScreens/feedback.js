import React, {Component} from 'react';
import {Platform, StyleSheet, Text, TextInput,View,SafeAreaView,ImageBackground, Image,TouchableOpacity} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
 
export default class Feedback extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expand : false,
      icon : require('../../asset/icon/home/up.png'),
      option: null,
      confirm: "Confirmed off-load",
      notConfirm: "Not Confirmed off-load",
      ticked1: true,
      ticked1: false,
      ticked1: false,
      ticked1: false,
    };  
  }
  onChangeOption = () =>{
    if(this.state.expand == false){
      this.setState({
        expand: true,
        icon : require('../../asset/icon/home/down.png'),
        option:
        (
          <View style={{paddingLeft: 18, borderTopColor: '#00000028', borderTopWidth: 1,alignItems: 'center',paddingRight:18, width: '100%', height: '50%', flexDirection: 'row'}}>
            <TouchableOpacity
          
            onPress={()=> this.setState({
              expand: false,
              icon : require('../../asset/icon/home/up.png'),
              option: null,
              confirm: this.state.notConfirm,
              notConfirm: this.state.confirm
            })}
            >
              <Text style={{color:"#000000", fontSize: 17}}>{this.state.notConfirm}</Text>
            </TouchableOpacity>
          </View>   
        )
      })
    }else{
      this.setState({
        expand: false,
        icon : require('../../asset/icon/home/up.png'),
        option: null,
      })
    }
  }
  render() {
    return (
        <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
          <SafeAreaView>
            <View style={{marginTop: Platform.OS === "ios" ? 0 : 50, width: "100%", alignItems: 'center',height: "5%", flexDirection: 'row'}}>
              <TouchableOpacity 
                style={{width: '10%', alignItems: 'center', height: '100%', justifyContent: 'center'}}
                onPress={() => this.props.navigation.goBack()}> 
                <Image source={require('../../asset/icon/profile/backbtn.png')} 
                style={{width: 10, height: 17,resizeMode: 'stretch', justifyContent: 'center'}}/>
              </TouchableOpacity>                
              <View style={{height: '100%', width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                <Text style={styles.title}>Feedback</Text>
              </View>
            </View>
            <View style={{height: '100%', width:"100%",padding: 10,backgroundColor: '#ffffff', marginTop: Platform.OS === "ios" ? 20 : 30, borderTopLeftRadius: 20, borderTopRightRadius: 18}}>
                <View style={{width: '100%', height: '10%', marginTop: 20}}>
                  <View style={{width: '100%', height: this.state.expand ?"100%":'80%',justifyContent: 'center',shadowOpacity: 1,shadowOffset: { width: 1, height: 2 }, shadowRadius: 2, elevation: 4, borderRadius: 22}}>
                    <View style={{justifyContent: 'space-between', paddingLeft: 18, paddingRight:18, alignItems: 'center',width: '100%', height: '50%', flexDirection: 'row'}}>
                      <TouchableOpacity onPress={() => this.onChangeOption()}>
                        <Text style={{color:"#000000", fontSize: 17}}>{this.state.confirm}</Text>
                        </TouchableOpacity>
                        <Image source={this.state.icon} style={{width: 12, height: 12}}/>
                    </View>
                    {this.state.option}
                  </View>
                </View>
                <View style={{width: '100%', height: '20%'}}>
                  <View>
                    <View style={{flexDirection: 'row', width: '100%', height: '15%'}}>
                      <TouchableOpacity onPress={() => this.setState({ticked1: true, ticked2: false, ticked3: false, ticked4: false})}>
                        <Image source={this.state.ticked1 ? require('../../asset/icon/home/ticked.png') : require('../../asset/icon/home/untick.png')} style={{width: 20, height: 20, marginLeft: 15, resizeMode: 'stretch'}}/>
                      </TouchableOpacity>
                      <Text style={{marginLeft: 15, fontSize: 18}}>Confirm arrival at customer place</Text>
                    </View>
                    <View style={{flexDirection: 'row', width: '100%', height: '15%', marginTop: 20}}>
                      <TouchableOpacity onPress={() => this.setState({ticked1: false, ticked2: true, ticked3: false, ticked4: false})}>
                        <Image source={this.state.ticked2 ? require('../../asset/icon/home/ticked.png') : require('../../asset/icon/home/untick.png')} style={{width: 20, height: 20, marginLeft: 15, resizeMode: 'stretch'}}/>
                      </TouchableOpacity>                      
                      <Text style={{marginLeft: 15, fontSize: 18}}>Confirm ready for customer inspection</Text>
                    </View>
                    <View style={{flexDirection: 'row', width: '100%', height: '15%', marginTop: 20}}>
                      <TouchableOpacity onPress={() => this.setState({ticked1: false, ticked2: false, ticked3: true, ticked4: false})}>
                        <Image source={this.state.ticked3 ? require('../../asset/icon/home/ticked.png') : require('../../asset/icon/home/untick.png')} style={{width: 20, height: 20, marginLeft: 15, resizeMode: 'stretch'}}/>
                      </TouchableOpacity>                      
                      <Text style={{marginLeft: 15, fontSize: 18}}>Confirmed inspection</Text>
                    </View>
                    <View style={{flexDirection: 'row', width: '100%', height: '15%', marginTop: 20}}>
                      <TouchableOpacity onPress={() => this.setState({ticked1: false, ticked2: false, ticked3: false, ticked4: true})}>
                        <Image source={this.state.ticked4 ? require('../../asset/icon/home/ticked.png') : require('../../asset/icon/home/untick.png')} style={{width: 20, height: 20, marginLeft: 15, resizeMode: 'stretch'}}/>
                      </TouchableOpacity>                      
                      <Text style={{marginLeft: 15, fontSize: 18}}>Confirm return</Text>
                    </View>
                  </View>
                </View>  
                <View  style={{padding: 15, width: '100%', height: '20%', marginTop: 20, shadowRadius: 2, elevation: 1, borderRadius: 15}}>
                  <TextInput 
                    placeholder="Note box"
                  />
                </View>
                <View style={{width: '100%', height: '10%', alignItems: 'flex-end' ,marginTop: 20}}>
                  <TouchableOpacity style={{backgroundColor: '#F0BC45', width: '33%', padding: 15,borderRadius: 22, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{color:'#000000', fontSize: 15}}>Send</Text>
                  </TouchableOpacity>
                </View>
            </View>
          </SafeAreaView>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%',  
  },
  mapContainer: {
    flex: 1,
    marginTop: 20,
    borderRadius: 20
  },
  sendIssue: {
    width: '100%',
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    padding: 15
  },
  confirmRecive: {
    width: '100%',
    backgroundColor: '#F0BC45',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    padding: 15, 
    marginTop: 16
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  },
  infoDetailContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
    borderRadius: 20
  },
  quantityContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 25
  },
  detail: {
    flex: 2,
    borderTopWidth: 1,
    borderTopColor: '#00000028',
    paddingTop: 26,
    paddingBottom: 26
  },
  txtContainer: {
    justifyContent: 'center',
    flex: 1,
    paddingLeft: 14
  },
})