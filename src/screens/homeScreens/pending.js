import React, {Component} from 'react';
import {Platform, StyleSheet, Text, Alert,Modal, TextInput, ScrollView,View,SafeAreaView,ImageBackground, Image,TouchableOpacity, StatusBar} from 'react-native';
 
export default class Pending extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expand : false,
      icon : require('../../asset/icon/home/up.png'),
      data : null,
      Alert_Visibility: false 
    };
  }
   
  Show_Custom_Alert(visible) {
    this.setState({Alert_Visibility: visible}); 
  }

  ok_Button=()=>{
    Alert.alert("OK Button Clicked.");
  }
  onPageChange = (position) => {
    this.setState({currentPosition: position});
  }

  expand_collapse_Function = () =>
  {
    if( this.state.expand == false )
    {
      this.setState({ 
        expand : true, 
        icon : require('../../asset/icon/home/down.png'),
        data : (
        <View>
          <View style={{width: '50%', height: 60, flexDirection: 'row', marginTop: 30}}>
            <View style={{alignItems: 'center'}}>
              <View style={{width: 20, height: 20, borderRadius: 10, borderWidth: 2, borderColor: '#707070', backgroundColor: '#ffffff'}}/>
              <View style={{width: 2, height: 40, backgroundColor: '#000000'}}/>
            </View>
            <View style={{marginLeft: 10}}>
              <Text>Pending</Text>
            </View>
          </View>
          <View style={{width: '50%', height: 60, flexDirection: 'row'}}>
            <View style={{alignItems: 'center'}}>
              <View style={{width: 20, height: 20, borderRadius: 10, borderWidth: 2, borderColor: '#707070', backgroundColor: '#ffffff'}}/>
              <View style={{width: 2, height: 40, backgroundColor: '#000000'}}/>
            </View>
            <View style={{marginLeft: 10}}>
              <Text>Up Coming</Text>
            </View>
          </View>
          <View style={{width: '50%', height: 60, flexDirection: 'row'}}>
            <View style={{alignItems: 'center'}}>
              <View style={{width: 20, height: 20, borderRadius: 10, borderWidth: 2, borderColor: '#707070', backgroundColor: '#ffffff'}}/>
              <View style={{width: 2, height: 40, backgroundColor: '#000000'}}/>
            </View>
            <View style={{marginLeft: 10}}>
              <Text>Arrival</Text>
            </View>
          </View>
          <View style={{width: '50%', height: 60, flexDirection: 'row'}}>
            <View style={{alignItems: 'center'}}>
              <View style={{width: 20, height: 20, borderRadius: 10, borderWidth: 2, borderColor: '#707070', backgroundColor: '#ffffff'}}/>
            </View>
            <View style={{marginLeft: 10}}>
              <Text>Completed</Text>
            </View>
          </View>
        </View>)
      }); 
    }
    else
    {
      this.setState({ 
        expand : false, 
        icon : require('../../asset/icon/home/up.png'),
        data: null
      });
    }
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
          <ScrollView style={{width: '100%',height:'100%'}}>
            <SafeAreaView>
              <View style={{marginBottom: 100, paddingLeft: 10, paddingRight: 10}}>
              <View style={{marginTop: Platform.OS === "ios" ? 0 : 50, width: "100%", alignItems: 'center',height: "5%", flexDirection: 'row'}}>
                <TouchableOpacity 
                style={{width: '10%', alignItems: 'center', height: '100%', justifyContent: 'center'}}
                onPress={() => this.props.navigation.goBack()}> 
                <Image source={require('../../asset/icon/profile/backbtn.png')} 
                style={{width: 10, height: 17,resizeMode: 'stretch'}}/>
                </TouchableOpacity>                
                <View style={{height: '100%', width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={styles.title}>Coming Up</Text>
                </View>
              </View>
                <View style={{ flex: 1,marginTop: Platform.OS === "ios" ? 20 : 30}}>
                  <View style={styles.infoDetailContainer}>
                    <View style={styles.quantityContainer}>
                      <Text style={{color:'#000000', fontSize: 20}}>Total Quantity</Text>
                      <Text style={{color: '#FF3400', fontWeight: 'bold',fontSize: 24}}>50, 000 L</Text>
                    </View>
                    <View style={styles.detail}>
                      <View style={styles.txtContainer}>
                        <Text style={{color: '#707070'}}>Detail</Text>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: 25,justifyContent: 'space-around',  paddingRight: 13,paddingLeft: 13 ,height: 175,width: '100%'}}>
                        <View style={{flex: 1, alignItems: 'center', shadowOpacity: 1, shadowOffset: { width: 1, height: 2 },shadowRadius: 2,elevation: 3,shadowColor: 'rgba(0, 0, 0, 0.26)', padding: 20,backgroundColor: '#ffffff', borderRadius: 15,marginRight: 10}}>
                          <View style={{flex: 1}}>
                            <Text style={styles.txt}>Booking Date</Text>
                          </View>
                          <View style={{flex: 1, alignItems: 'center', marginTop: 20}}>
                            <Text style={{fontWeight: 'bold',color: '#000000', fontSize: 21}}>24</Text>
                            <Text style={{color: '#707070', marginTop: 8}}>April</Text>
                            </View>
                          <View style={{justifyContent: 'flex-end', flex: 1}}>
                            <Text style={{color: '#000000'}}>9:35 AM</Text>
                          </View>
                        </View>
                        <View style={{flex: 1, alignItems: 'center',shadowOpacity: 1, shadowOffset: { width: 2, height: 2 },shadowRadius: 3,elevation: 3,shadowColor: 'rgba(0, 0, 0, 0.25)', padding: 20,backgroundColor: '#ffffff', borderRadius: 12, marginLeft: 10}}>
                          <View style={{flex: 1}}>
                            <Text style={styles.txt}>Exspected Date</Text>
                          </View>
                          <View style={{flex: 1, alignItems: 'center', marginTop: 20}}>
                            <Text style={{fontWeight: 'bold',color: '#000000', fontSize: 21}}>25</Text>
                            <Text style={{color: '#707070',marginTop: 8}}>April</Text>
                            </View>
                          <View style={{justifyContent: 'flex-end', flex: 1}}>
                            <Text style={{color: '#000000'}}>3 : 00 PM</Text>
                          </View>
                        </View>
                      </View>
                      <View style={{flex: 2,paddingLeft: 16, paddingRight: 16, marginTop: 25}}>
                        <View style={{backgroundColor: '#0000000D', padding: 9, marginTop: 3,flexDirection: 'row', alignItems: 'center',borderRadius: 7}}>
                          <Image source={require('../../asset/icon/home/yellow.png')} style={{width: 27,height: 27, resizeMode: 'contain'}}/>
                          <View style={{marginLeft: 15}}>
                            <Text style={{color: '#000000',fontSize: 16}}>Diesel</Text>
                            <Text style={{color: '#707070', fontSize: 11}}>00024</Text>
                          </View>
                          <View style={{flex: 1, alignItems: 'flex-end'}}>
                            <Text style={{color: '#6A6A6A',fontSize: 18, fontWeight: 'bold'}}>1 000 L</Text>
                          </View>
                        </View>
                        <View style={{backgroundColor: '#0000000D', padding: 9, marginTop: 3,flexDirection: 'row', alignItems: 'center',borderRadius: 7}}>
                          <Image source={require('../../asset/icon/home/red.png')} style={{width: 27,height: 27, resizeMode: 'contain'}}/>
                          <View style={{marginLeft: 15}}>
                            <Text style={{color: '#000000',fontSize: 16}}>Petrol</Text>
                            <Text style={{color: '#707070', fontSize: 11}}>00025</Text>
                          </View>
                          <View style={{flex: 1, alignItems: 'flex-end'}}>
                            <Text style={{color: '#6A6A6A',fontSize: 18, fontWeight: 'bold'}}>1 500 L</Text>
                          </View>
                        </View>
                        <View style={{backgroundColor: '#0000000D', padding: 9, marginTop: 3,flexDirection: 'row', alignItems: 'center',borderRadius: 7}}>
                          <Image source={require('../../asset/icon/home/blue.png')} style={{width: 27,height: 27, resizeMode: 'contain'}}/>
                          <View style={{marginLeft: 15}}>
                            <Text style={{color: '#000000',fontSize: 16}}>Lubricating Oil</Text>
                            <Text style={{color: '#707070', fontSize: 11}}>00026</Text>
                          </View>
                          <View style={{flex: 1, alignItems: 'flex-end'}}>
                            <Text style={{color: '#6A6A6A',fontSize: 18, fontWeight: 'bold'}}>500 L</Text>
                          </View>
                        </View>
                        <View style={{backgroundColor: '#0000000D', padding: 9, marginTop: 3,flexDirection: 'row', alignItems: 'center',borderRadius: 7}}>
                          <Image source={require('../../asset/icon/home/green.png')} style={{width: 27,height: 27, resizeMode: 'contain'}}/>
                          <View style={{marginLeft: 15}}>
                            <Text style={{color: '#000000',fontSize: 16}}>Kerosene</Text>
                            <Text style={{color: '#707070', fontSize: 11}}>00027</Text>
                          </View>
                          <View style={{flex: 1, alignItems: 'flex-end'}}>
                            <Text style={{color: '#6A6A6A',fontSize: 18, fontWeight: 'bold'}}>1 5000 L</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View  style={{backgroundColor: '#ffffff', padding: 15, borderRadius: 20, marginTop: 20}}>
                  <TouchableOpacity onPress={() =>{
                    this.expand_collapse_Function()
                  }}
                    style={{flex: 1, alignItems: 'center', flexDirection: 'row'}}>
                    <Text style={{color: '#000000', fontSize: 16}}>Tracking your delivery process</Text>
                    <View style={{width: 12, height: 12, flex: 1, alignItems: 'flex-end'}}>
                      <Image source={this.state.icon}
                      style={{width: 12, height: 12}}/>
                    </View>
                  </TouchableOpacity>
                  <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1}}>
                    {this.state.data}
                  </View>
                </View>
                <Modal 
                  visible={this.state.Alert_Visibility}
                  transparent={true}
                  animationType={"fade"}
                  onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Alert_Visibility)} } >
                  <StatusBar backgroundColor = 'rgba(52, 52, 52, 0.8)'/>
                    <View style={{ flex:1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(52, 52, 52, 0.8)'}}>
                        <View style={styles.Alert_Main_View}> 
                          <View style={{width: '100%', height: '50%',justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={styles.TextStyle}> Are you sure to cancel ?</Text>
                          </View>
                          <View style={{flexDirection: 'row',width: '100%', height: '50%', justifyContent: 'space-around',alignItems: 'flex-end'}}>
                            <TouchableOpacity 
                              style={{justifyContent: 'center', alignItems: 'center', width: '45%', height: '70%',backgroundColor: '#f1f1f1', borderRadius: 16}}
                              onPress={this.ok_Button} 
                              activeOpacity={0.7}>
                              <Text> YES </Text>
                            </TouchableOpacity> 
                            <TouchableOpacity 
                              style={{justifyContent: 'center', alignItems: 'center', width: '45%', height: '70%',backgroundColor: '#f2bb5b', borderRadius: 16}}
                              onPress={() => { this.Show_Custom_Alert(!this.state.Alert_Visibility)} } 
                              activeOpacity={0.7}>
                              <Text> NO </Text>
                            </TouchableOpacity>
                          </View>                
                        </View>
                    </View>
                </Modal>
                <View style={styles.btnContainer}> 
                  <TouchableOpacity 
                   onPress={() => { this.Show_Custom_Alert(true) }} 
                    style={styles.sendIssue}>
                    <Text>Cancel Booking</Text>
                  </TouchableOpacity>
                  <TouchableOpacity 
                  
                  style={styles.confirmRecive}>
                    <Text>Edit Booking</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </SafeAreaView>
          </ScrollView>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%',  
  },
  mapContainer: {
    flex: 1,
    marginTop: 30,
    borderRadius: 20
  },
  sendIssue: {
    width: '100%',
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    padding: 15
  },
  confirmRecive: {
    width: '100%',
    backgroundColor: '#F0BC45',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    padding: 15, 
    marginTop: 18
  },
  btnContainer: {
    flex: 1,
    marginTop: 18
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  },
  infoDetailContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
    borderRadius: 20
  },
  quantityContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 25
  },
  detail: {
    flex: 2,
    borderTopWidth: 1,
    borderTopColor: '#00000028',
    paddingTop: 26,
    paddingBottom: 26
  },
  txtContainer: {
    justifyContent: 'center',
    flex: 1,
    paddingLeft: 14
  },
  Alert_Main_View:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor : "#ffffff", 
    height: 130 ,
    width: '65%',
    borderRadius:8,
    padding: 20
   
  },
   
  TextStyle:{
      color:'#000000',
      fontSize: 15,
  }
})