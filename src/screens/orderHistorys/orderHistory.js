import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, SafeAreaView, ImageBackground,Image,TouchableOpacity, ScrollView, TextInput, } from 'react-native';

export default class OrderHistory extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
          <ScrollView style={{width: '100%',height:'100%'}}>
            <SafeAreaView>
              <View style={{marginBottom: 100, paddingLeft: 10, paddingRight: 10}}>
                <View style={{marginTop: Platform.OS === "ios"? 30 : 50, alignItems: 'center'}}>
                  <Text style={styles.title}>Order History</Text>
                </View>  
                <View style={{marginTop: Platform.OS === "ios" ? 20 : 30}}>
                  <View style={{width: '100%',borderRadius: 25, backgroundColor: '#ffffff',flexDirection: 'row', alignItems: 'center', padding: Platform.OS === "ios" ? 12 : 0}}>
                    <TouchableOpacity style={{width: 20, height: 20, marginLeft: Platform.OS === "ios" ? 8 : 20}}>
                      <Image source={require('../../asset/icon/home/search.png')} style={{width: 20, height: 20}}/>
                    </TouchableOpacity>
                    <TextInput 
                    placeholder= " search"
                    placeholderTextColor= "#6A6A6A"
                    style={{marginLeft: 8, marginRight: 20, fontSize: 16, color: '#6A6A6A', width: '100%'}}
                    />
                  </View>
                  <View style={{marginTop: 20}}>
                  <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('OrderHistoryDetailScreen')}
                    style={{width: '100%', height: 85, flexDirection: 'row',backgroundColor: '#ffffff', padding: 10, marginTop: 10, borderRadius: 21 }}>
                      <View style={{flex: 2, alignItems: 'flex-start'}}>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start',flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{height: 17, width: 15, resizeMode: 'contain'}}/>
                          <Text style={{color: '#000000', marginLeft: 10, fontSize: 17}}>Booking Date</Text>
                        </View>
                        <View style={{ flex: 1}}>
                          <Text style={{color: '#6A6A6A', fontSize: 12, marginLeft: 6}}>6 : 00 PM 24 April, 2020</Text>
                        </View>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View>
                          <Text style={{color: '#017C5F'}}>Confirmed</Text>
                        </View>
                        <View style={{justifyContent: 'flex-end', flex: 1}}>
                          <View style={{backgroundColor:'#f7e4c3', padding: 5,borderRadius: 10,}}>
                            <Text style={{color: '#707070' ,fontSize: 14}}>#000321</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: '100%', height: 85, flexDirection: 'row',backgroundColor: '#ffffff', padding: 10, marginTop: 10, borderRadius: 21 }}>
                      <View style={{flex: 2, alignItems: 'flex-start'}}>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start',flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{height: 17, width: 15, resizeMode: 'contain'}}/>
                          <Text style={{color: '#000000', marginLeft: 10, fontSize: 17}}>Booking Date</Text>
                        </View>
                        <View style={{ flex: 1}}>
                          <Text style={{color: '#6A6A6A', fontSize: 12, marginLeft: 6}}>6 : 00 PM 24 April, 2020</Text>
                        </View>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View>
                          <Text style={{color: '#017C5F'}}>Confirmed</Text>
                        </View>
                        <View style={{justifyContent: 'flex-end', flex: 1}}>
                          <View style={{backgroundColor:'#f7e4c3', padding: 5,borderRadius: 10,}}>
                            <Text style={{color: '#707070' ,fontSize: 14}}>#000321</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: '100%', height: 85, flexDirection: 'row',backgroundColor: '#ffffff', padding: 10, marginTop: 10, borderRadius: 21 }}>
                      <View style={{flex: 2, alignItems: 'flex-start'}}>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start',flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{height: 17, width: 15, resizeMode: 'contain'}}/>
                          <Text style={{color: '#000000', marginLeft: 10, fontSize: 17}}>Booking Date</Text>
                        </View>
                        <View style={{ flex: 1}}>
                          <Text style={{color: '#6A6A6A', fontSize: 12, marginLeft: 6}}>6 : 00 PM 24 April, 2020</Text>
                        </View>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View>
                          <Text style={{color: '#017C5F'}}>Confirmed</Text>
                        </View>
                        <View style={{justifyContent: 'flex-end', flex: 1}}>
                          <View style={{backgroundColor:'#f7e4c3', padding: 5,borderRadius: 10,}}>
                            <Text style={{color: '#707070' ,fontSize: 14}}>#000321</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: '100%', height: 85, flexDirection: 'row',backgroundColor: '#ffffff', padding: 10, marginTop: 10, borderRadius: 21 }}>
                      <View style={{flex: 2, alignItems: 'flex-start'}}>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start',flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{height: 17, width: 15, resizeMode: 'contain'}}/>
                          <Text style={{color: '#000000', marginLeft: 10, fontSize: 17}}>Booking Date</Text>
                        </View>
                        <View style={{ flex: 1}}>
                          <Text style={{color: '#6A6A6A', fontSize: 12, marginLeft: 6}}>6 : 00 PM 24 April, 2020</Text>
                        </View>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View>
                          <Text style={{color: '#017C5F'}}>Confirmed</Text>
                        </View>
                        <View style={{justifyContent: 'flex-end', flex: 1}}>
                          <View style={{backgroundColor:'#f7e4c3', padding: 5,borderRadius: 10,}}>
                            <Text style={{color: '#707070' ,fontSize: 14}}>#000321</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: '100%', height: 85, flexDirection: 'row',backgroundColor: '#ffffff', padding: 10, marginTop: 10, borderRadius: 21 }}>
                      <View style={{flex: 2, alignItems: 'flex-start'}}>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start',flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{height: 17, width: 15, resizeMode: 'contain'}}/>
                          <Text style={{color: '#000000', marginLeft: 10, fontSize: 17}}>Booking Date</Text>
                        </View>
                        <View style={{ flex: 1}}>
                          <Text style={{color: '#6A6A6A', fontSize: 12, marginLeft: 6}}>6 : 00 PM 24 April, 2020</Text>
                        </View>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View>
                          <Text style={{color: '#017C5F'}}>Confirmed</Text>
                        </View>
                        <View style={{justifyContent: 'flex-end', flex: 1}}>
                          <View style={{backgroundColor:'#f7e4c3', padding: 5,borderRadius: 10,}}>
                            <Text style={{color: '#707070' ,fontSize: 14}}>#000321</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: '100%', height: 85, flexDirection: 'row',backgroundColor: '#ffffff', padding: 10, marginTop: 10, borderRadius: 21 }}>
                      <View style={{flex: 2, alignItems: 'flex-start'}}>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start',flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{height: 17, width: 15, resizeMode: 'contain'}}/>
                          <Text style={{color: '#000000', marginLeft: 10, fontSize: 17}}>Booking Date</Text>
                        </View>
                        <View style={{ flex: 1}}>
                          <Text style={{color: '#6A6A6A', fontSize: 12, marginLeft: 6}}>6 : 00 PM 24 April, 2020</Text>
                        </View>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View>
                          <Text style={{color: '#017C5F'}}>Confirmed</Text>
                        </View>
                        <View style={{justifyContent: 'flex-end', flex: 1}}>
                          <View style={{backgroundColor:'#f7e4c3', padding: 5,borderRadius: 10,}}>
                            <Text style={{color: '#707070' ,fontSize: 14}}>#000321</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: '100%', height: 85, flexDirection: 'row',backgroundColor: '#ffffff', padding: 10, marginTop: 10, borderRadius: 21 }}>
                      <View style={{flex: 2, alignItems: 'flex-start'}}>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start',flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{height: 17, width: 15, resizeMode: 'contain'}}/>
                          <Text style={{color: '#000000', marginLeft: 10, fontSize: 17}}>Booking Date</Text>
                        </View>
                        <View style={{ flex: 1}}>
                          <Text style={{color: '#6A6A6A', fontSize: 12, marginLeft: 6}}>6 : 00 PM 24 April, 2020</Text>
                        </View>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View>
                          <Text style={{color: '#017C5F'}}>Confirmed</Text>
                        </View>
                        <View style={{justifyContent: 'flex-end', flex: 1}}>
                          <View style={{backgroundColor:'#f7e4c3', padding: 5,borderRadius: 10,}}>
                            <Text style={{color: '#707070' ,fontSize: 14}}>#000321</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>    
                    <TouchableOpacity style={{width: '100%', height: 85, flexDirection: 'row',backgroundColor: '#ffffff', padding: 10, marginTop: 10, borderRadius: 21 }}>
                      <View style={{flex: 2, alignItems: 'flex-start'}}>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start',flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{height: 17, width: 15, resizeMode: 'contain'}}/>
                          <Text style={{color: '#000000', marginLeft: 10, fontSize: 17}}>Booking Date</Text>
                        </View>
                        <View style={{ flex: 1}}>
                          <Text style={{color: '#6A6A6A', fontSize: 12, marginLeft: 6}}>6 : 00 PM 24 April, 2020</Text>
                        </View>
                      </View>
                      <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View>
                          <Text style={{color: '#017C5F'}}>Confirmed</Text>
                        </View>
                        <View style={{justifyContent: 'flex-end', flex: 1}}>
                          <View style={{backgroundColor:'#f7e4c3', padding: 5,borderRadius: 10,}}>
                            <Text style={{color: '#707070' ,fontSize: 14}}>#000321</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </SafeAreaView>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%'
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  },
  
})