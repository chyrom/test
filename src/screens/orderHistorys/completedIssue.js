import React, {Component} from 'react';
import {Platform, StyleSheet, Text, TextInput,View,SafeAreaView,ImageBackground, Image,TouchableOpacity} from 'react-native';

export default class CompletedIssue extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={require('../../asset/icon/orderhistory/bg.png')} style={styles.backgroundImg}>
          <SafeAreaView>
            <View style={{width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
              <View style={{height: 300, height: 300}}>
                <Image source={require('../../asset/icon/orderhistory/success.png')} style={{width: 200, height: 200}}/>
              </View>
              <Text style={[styles.title], {marginTop: 35}}>Thank for your Issue!</Text>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%',  
  },
  title: {
    color: '#000000',
    fontSize: 60,
  }


})