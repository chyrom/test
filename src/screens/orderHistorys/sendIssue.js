import React, {Component} from 'react';
import {Platform, StyleSheet, Text, TextInput,View,SafeAreaView,ImageBackground, Image,TouchableOpacity} from 'react-native';
 
export default class SendIssue extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={require('../../asset/icon/orderhistory/bg.png')} style={styles.backgroundImg}>
          <SafeAreaView>
            <View style={{paddingLeft: 10, paddingRight: 10}}>  
              <View style={{marginTop: Platform.OS === "ios" ? 20 : 50, width: "100%", alignItems: 'center',flexDirection: 'row', padding: 5}}>
                <TouchableOpacity 
                style={{width: '10%', justifyContent: 'center'}}
                onPress={() => this.props.navigation.goBack()}> 
                <Image source={require('../../asset/icon/profile/backbtn.png')} 
                style={{width: 10, height: 17,resizeMode: 'stretch'}}/>
                </TouchableOpacity>                
                <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={styles.title}>Send Issue</Text>
                </View>
              </View>
              <View style={{backgroundColor: '#ffffff', borderRadius: 10, marginTop: 50,width: '100%', height: 260, shadowOpacity: 1, shadowOffset: { width: 1, height: 2 },shadowRadius: 2,elevation: 3,shadowColor: 'rgba(0, 0, 0, 0.26)'}}>
                <View style={{padding: 20, flex: 3, borderTopRightRadius: 10, borderTopLeftRadius: 10}}>
                  <TextInput 
                  placeholder= "Write your issue here..."
                  placeholderTextColor= "#6A6A6A"
                  />
                </View>
                <View style={{borderTopWidth: 1, borderBottomLeftRadius: 10, borderBottomRightRadius: 10,flex: 0.8, alignItems:'center', justifyContent: 'flex-end', borderTopColor: '#00000028', flexDirection: 'row'}}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.goBack()} 
                    style={{ padding: 8, marginRight: 20}}>
                    <Text style={{color: '#6A6A6A'}}>Cancel</Text>
                  </TouchableOpacity>
                  <TouchableOpacity 
                  onPress={() => this.props.navigation.navigate('CompletedIssueScreen')}
                  style={{ padding: 8,marginRight: 24}}>
                    <Text style={{color: '#6A6A6A'}}>Send</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View> 
          </SafeAreaView>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%',  
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  }


})