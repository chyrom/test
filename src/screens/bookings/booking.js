import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,SafeAreaView, ScrollView,TouchableOpacity,TextInput,ImageBackground,Image} from 'react-native';

export default class Booking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMaintenace: true,
      isInspection: false,
    };
  }
  render() {
    return (
      <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
        <ScrollView style={{width: '100%', height: '100%'}}>
        <SafeAreaView>
          <View style={{width: '100%',height: '100%', paddingLeft: 10, paddingRight: 10, marginBottom: 100}}> 
            <View style={{ marginTop: Platform.OS === "ios"? 20 : 20, flex: 1}}>
              <View style={{flexDirection:"row",alignSelf:'center'}}>
                <Text style={{color:'green',fontWeight:'bold',fontSize:20, alignSelf:'center'}}>bvm</Text>
                <Text style={{color:'#000000',fontSize:20,alignSelf:'center',marginLeft:8}}>pretroleom</Text>
              </View>
              <View style={{flex: 0.2, marginTop: 25}}>
                <View style={{width: '100%', height: 98, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <TouchableOpacity onPress={() => this.setState({isMaintenace: true, isInspection: false})}
                    style={{width: '48.5%', height: '100%', backgroundColor: this.state.isMaintenace ? '#ffffff' : "#e4dbc2", borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                    <Image source={this.state.isMaintenace? require('../../asset/icon/booking/activemaintenance.png'): require('../../asset/icon/booking/inactiveMaintenance.png')} style={{width: 35, height: 35}}/>
                    <Text>Maintenance</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.setState({isMaintenace: false, isInspection: true})}
                    style={{width: '48.5%', height: '100%', backgroundColor: this.state.isInspection ? '#ffffff' : "#e4dbc2", borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                    <Image source={this.state.isInspection? require('../../asset/icon/booking/activeinspection.png'):require('../../asset/icon/booking/inactiveInspection.png')} style={{width: 35, height: 35, resizeMode: 'stretch'}}/>
                    <Text>Inspection</Text>
                  </TouchableOpacity>
                </View>
              </View>
              {this.state.isMaintenace? 
              <View style={{flex: 1}}>
                <View style={{width: '100%', height: 150, backgroundColor:'#ffffff', flexDirection: 'row', padding: 18, borderRadius: 16}}>
                  <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start'}}>
                    <Image source={require('../../asset/icon/booking/motoIdentity.png')} style={{width: '100%', height: 85, borderRadius: 2, resizeMode: 'stretch'}}/>
                  </View>
                  <View style={{flex: 1, paddingLeft: 20, paddingRight: 15}}> 
                    <Text style={{color: '#000000', fontSize: 16}}>Fuel Tangker-Juzz-Mini van</Text>
                    <Text style={{color: '#6A6A6A', marginTop: 17}}>Oct 08 2019</Text>
                  </View>
                  <View>
                    <TouchableOpacity>
                      <Image source={require('../../asset/icon/booking/seemoreIcon.png')} style={{width: 4, height: 18, resizeMode: 'stretch'}}/>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>: null}
              {this.state.isInspection? 
              <View style={{flex: 1}}>
                <View style={{width: '100%', height: 150, backgroundColor:'#ffffff', flexDirection: 'row', padding: 18, borderRadius: 16}}>
                  <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start'}}>
                    <Image source={require('../../asset/icon/booking/motoIdentity.png')} style={{width: 155, height: 85, borderRadius: 2, resizeMode: 'stretch'}}/>
                  </View>
                  <View style={{flex: 1, paddingLeft: 20, paddingRight: 15}}> 
                    <Text style={{color: '#000000', fontSize: 16}}>Fuel Tangker - Juzz - Mini van</Text>
                    <Text style={{color: '#6A6A6A', marginTop: 17}}>Oct 08 2019</Text>
                    <View style={{flex: 1, justifyContent: 'flex-end',}}>
                      <Text style={{color: '#DC9900', fontSize: 16}}>Pending</Text>
                    </View>
                  </View>
                  <View>
                    <TouchableOpacity>
                      <Image source={require('../../asset/icon/booking/seemoreIcon.png')} style={{width: 4, height: 18, resizeMode: 'stretch'}}/>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>: null}
            </View>
          </View>
        </SafeAreaView>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%'
  },
  cameraContainer:{
    width:40,
    height:40,
    borderRadius:20,
    position:'absolute',
    bottom:0,
    right:0
  },
  txt: {
    color: '#000000',
    fontSize: 18,
  },
  itemtxt: {
    marginTop: 10,
    fontSize: 15,
    color: '#6A6A6A'
  },
  
})