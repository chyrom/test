import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, SafeAreaView, FlatList,Modal, ImageBackground,Image,TouchableOpacity, ScrollView, TextInput } from 'react-native';

export default class TruckSuggestion extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
          <ScrollView style={{width: '100%',height:'100%'}}>
            <SafeAreaView>
              <View style={{paddingLeft: 10, paddingRight: 10, flex: 1, marginBottom: 500}}>
                <View style={{marginTop: Platform.OS === "ios" ? 10 : 23, width: "100%", alignItems: 'center', height: "10%",flexDirection: 'row'}}>
                  <TouchableOpacity 
                  style={{width: '10%', justifyContent: 'center', height: '100%'}}
                  onPress={() => this.props.navigation.goBack()}> 
                  <Image source={require('../../asset/icon/profile/backbtn.png')} 
                  style={{width: 10, height: 15,resizeMode: 'stretch', marginLeft: 10}}/>
                  </TouchableOpacity>                
                  <View style={{height: '100%', width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.title}>Truck Suggestion</Text>
                  </View>
                </View>
                <View style={{marginTop: Platform.OS === "ios" ? 20 : 15, flex: 1}}>
                  <View style={{width: '100%', height: '16%'}}>
                    <View style={{width: '100%', height: 70}}>                
                      <TouchableOpacity style={{width: '100%', height: 82, backgroundColor: '#ffffff', borderRadius: 12, padding: 14}}>
                        <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{width: 13, height: 16, resizeMode: 'stretch'}}/>
                          <Text style={{color: '#000000', fontSize: 16, marginLeft: 8}}>Kompong Speu</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'flex-end'}}>
                          <Text style={{marginLeft: 18, color: '#707070'}}># NR4, krong Chbar Mon</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('TruckSuggestionDetailScreen')}
                    style={{width: '100%', height: "30%", backgroundColor: '#ffffff', borderRadius: 12, padding: 16}}>
                    <View style={{flexDirection: 'row', width: '100%', height: '40%', justifyContent: 'space-between'}}>
                      <View style={{width: '50%', height: '100%', flexDirection: 'row'}}>
                        <View style={{alignItems: 'center'}}>
                          <Image source={require('../../asset/icon/booking/car.png')} style={{width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </View>
                        <View style={{marginLeft: 10}}>
                          <Text style={{color: '#000000', fontSize: 16, fontWeight: 'bold'}}>Phnom Penh</Text>
                          <Text style={{color: '#00468B', fontSize: 16, fontWeight: 'bold',marginTop: 4}}>4U - 9999</Text>
                        </View>
                      </View>
                      <View style={{width: '50%', height: '100%', alignItems: 'flex-end'}}>
                        <Text style={{color: '#000000', fontSize: 16}}>Kompong Speu</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 4}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{width: 11, height: 14, resizeMode: 'stretch', marginRight: 10}}/>
                          <Text style={{color: "#FF3400", fontSize: 14}}>Direction</Text>
                        </View>
                      </View>
                    </View>
                    <View style={{width: '100%', height: '55%', flexDirection: 'row', borderRadius: 10, backgroundColor: '#e8e8e8', padding: 10}}>
                      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight:'bold', color: '#000000', fontSize: 17}}>10 000L</Text>
                        <Text style={{color: '#707070', fontSize: 13}}>Total</Text>
                      </View>
                      <View style={{height: '100%', width: 1, backgroundColor: '#ffffff'}}/>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontWeight:'bold', color: '#FF3400', fontSize: 17}}>5 000L</Text>
                        <Text style={{color: '#707070', fontSize: 13}}>Avaiable</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width: '100%', height: "30%", marginTop: 17, backgroundColor: '#ffffff', borderRadius: 12, padding: 16}}>
                    <View style={{flexDirection: 'row', width: '100%', height: '40%', justifyContent: 'space-between'}}>
                      <View style={{width: '50%', height: '100%', flexDirection: 'row'}}>
                        <View style={{alignItems: 'center'}}>
                          <Image source={require('../../asset/icon/booking/car.png')} style={{width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </View>
                        <View style={{marginLeft: 10}}>
                          <Text style={{color: '#000000', fontSize: 16, fontWeight: 'bold'}}>Phnom Penh</Text>
                          <Text style={{color: '#00468B', fontSize: 16, fontWeight: 'bold',marginTop: 4}}>4U - 9999</Text>
                        </View>
                      </View>
                      <View style={{width: '50%', height: '100%', alignItems: 'flex-end'}}>
                        <Text style={{color: '#000000', fontSize: 16}}>Kompong Speu</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 4}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{width: 11, height: 14, resizeMode: 'stretch', marginRight: 10}}/>
                          <Text style={{color: "#FF3400", fontSize: 14}}>Direction</Text>
                        </View>
                      </View>
                    </View>
                    <View style={{width: '100%', height: '55%', flexDirection: 'row', borderRadius: 10, backgroundColor: '#e8e8e8', padding: 10}}>
                      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight:'bold', color: '#000000', fontSize: 17}}>10 000L</Text>
                        <Text style={{color: '#707070', fontSize: 13}}>Total</Text>
                      </View>
                      <View style={{height: '100%', width: 1, backgroundColor: '#ffffff'}}/>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontWeight:'bold', color: '#FF3400', fontSize: 17}}>5 000L</Text>
                        <Text style={{color: '#707070', fontSize: 13}}>Avaiable</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width: '100%', height: "30%", marginTop: 17, backgroundColor: '#ffffff', borderRadius: 12, padding: 16}}>
                    <View style={{flexDirection: 'row', width: '100%', height: '40%', justifyContent: 'space-between'}}>
                      <View style={{width: '50%', height: '100%', flexDirection: 'row'}}>
                        <View style={{alignItems: 'center'}}>
                          <Image source={require('../../asset/icon/booking/car.png')} style={{width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </View>
                        <View style={{marginLeft: 10}}>
                          <Text style={{color: '#000000', fontSize: 16, fontWeight: 'bold'}}>Phnom Penh</Text>
                          <Text style={{color: '#00468B', fontSize: 16, fontWeight: 'bold',marginTop: 4}}>4U - 9999</Text>
                        </View>
                      </View>
                      <View style={{width: '50%', height: '100%', alignItems: 'flex-end'}}>
                        <Text style={{color: '#000000', fontSize: 16}}>Kompong Speu</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 4}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{width: 11, height: 14, resizeMode: 'stretch', marginRight: 10}}/>
                          <Text style={{color: "#FF3400", fontSize: 14}}>Direction</Text>
                        </View>
                      </View>
                    </View>
                    <View style={{width: '100%', height: '55%', flexDirection: 'row', borderRadius: 10, backgroundColor: '#e8e8e8', padding: 10}}>
                      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight:'bold', color: '#000000', fontSize: 17}}>10 000L</Text>
                        <Text style={{color: '#707070', fontSize: 13}}>Total</Text>
                      </View>
                      <View style={{height: '100%', width: 1, backgroundColor: '#ffffff'}}/>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontWeight:'bold', color: '#FF3400', fontSize: 17}}>5 000L</Text>
                        <Text style={{color: '#707070', fontSize: 13}}>Avaiable</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width: '100%', height: "30%", marginTop: 17, backgroundColor: '#ffffff', borderRadius: 12, padding: 16}}>
                    <View style={{flexDirection: 'row', width: '100%', height: '40%', justifyContent: 'space-between'}}>
                      <View style={{width: '50%', height: '100%', flexDirection: 'row'}}>
                        <View style={{alignItems: 'center'}}>
                          <Image source={require('../../asset/icon/booking/car.png')} style={{width: 40, height: 40, resizeMode: 'stretch'}}/>
                        </View>
                        <View style={{marginLeft: 10}}>
                          <Text style={{color: '#000000', fontSize: 16, fontWeight: 'bold'}}>Phnom Penh</Text>
                          <Text style={{color: '#00468B', fontSize: 16, fontWeight: 'bold',marginTop: 4}}>4U - 9999</Text>
                        </View>
                      </View>
                      <View style={{width: '50%', height: '100%', alignItems: 'flex-end'}}>
                        <Text style={{color: '#000000', fontSize: 16}}>Kompong Speu</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 4}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{width: 11, height: 14, resizeMode: 'stretch', marginRight: 10}}/>
                          <Text style={{color: "#FF3400", fontSize: 14}}>Direction</Text>
                        </View>
                      </View>
                    </View>
                    <View style={{width: '100%', height: '55%', flexDirection: 'row', borderRadius: 10, backgroundColor: '#e8e8e8', padding: 10}}>
                      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontWeight:'bold', color: '#000000', fontSize: 17}}>10 000L</Text>
                        <Text style={{color: '#707070', fontSize: 13}}>Total</Text>
                      </View>
                      <View style={{height: '100%', width: 1, backgroundColor: '#ffffff'}}/>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontWeight:'bold', color: '#FF3400', fontSize: 17}}>5 000L</Text>
                        <Text style={{color: '#707070', fontSize: 13}}>Avaiable</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </SafeAreaView>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%'
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  },
}) 

