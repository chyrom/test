import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, SafeAreaView, FlatList,Modal, ImageBackground,Image,TouchableOpacity, ScrollView, TextInput, YellowBox, } from 'react-native';

export default class TruckSuggestionDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expand: false,
      show_option: null,
      compartment1: "Compartment 01",
      compartment2: "Compartment 02"

    };  
  }
  show_expand =()=>{
    if(this.state.expand == false){
      this.setState({
        expand: true,
        show_option: (
        <View style={{width: '100%',height: '50%', justifyContent: 'space-between', alignItems: 'center', padding: 8, flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => this.setState({expand: false, show_option: null,compartment1: this.state.compartment2, compartment2: this.state.compartment1 })}>
          <Text style={{color: '#6A6A6A', fontSize: 13}}>{this.state.compartment2}</Text>
          </TouchableOpacity>
        </View>
      )
    })}
    else{
      this.setState({
        expand: false,
        show_option: null,
      })
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
            <SafeAreaView>
              <View style={{paddingLeft: 10, paddingRight: 10, width: '100%', height: '100%'}}>
              <View style={{marginTop: Platform.OS === "ios" ? 10 : 30, width: "100%", alignItems: 'center',height: "5%",flexDirection: 'row'}}>
                <TouchableOpacity 
                style={{width: '10%', justifyContent: 'center', height: '100%'}}
                onPress={() => this.props.navigation.goBack()}> 
                <Image source={require('../../asset/icon/profile/backbtn.png')} 
                style={{width: 10, height: 14,resizeMode: 'stretch'}}/>
                </TouchableOpacity>                
                <View style={{height: '100%', width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={styles.title}>Truck Suggestion</Text>
                </View>
              </View>
                <View style={{marginTop: Platform.OS === "ios" ? 20 : 20,width: '100%', height: Platform.OS === "ios" ? '78%' : '70%'}}>
                    <View style={{width: '100%', height: '16%'}}>                
                      <TouchableOpacity style={{width: '100%', height: "100%", backgroundColor: '#ffffff', borderRadius: 12, padding: 14}}>
                        <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                          <Image source={require('../../asset/icon/profile/direction.png')} style={{width: 13, height: 16, resizeMode: 'stretch'}}/>
                          <Text style={{color: '#000000', fontSize: 16, marginLeft: 8}}>Kompong Speu</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'flex-end'}}>
                          <Text style={{marginLeft: 18, color: '#707070'}}># NR4, krong Chbar Mon</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                    <View style={{width: '100%', marginTop: 30,height: "85%", backgroundColor: '#ffffff', borderRadius: 12, paddingLeft: 14, paddingRight: 14, paddingBottom: 20, paddingTop: 20}}>
                      <View style={{flexDirection: 'row', width: '100%', height: '17%', justifyContent: 'space-between'}}>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                          <View style={{alignItems: 'center'}}>
                            <Image source={require('../../asset/icon/booking/car.png')} style={{width: 40, height: 40, resizeMode: 'stretch'}}/>
                          </View>
                          <View style={{marginLeft: 10}}>
                            <Text style={{color: '#000000', fontSize: 16, fontWeight: 'bold'}}>Phnom Penh</Text>
                            <Text style={{color: '#00468B', fontSize: 16, fontWeight: 'bold',marginTop: 4}}>4U - 9999</Text>
                          </View>
                        </View>
                        <View style={{flex: 1, alignItems: 'flex-end'}}>
                          <Text style={{color: '#000000', fontSize: 16}}>Kompong Speu</Text>
                          <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                            <Image source={require('../../asset/icon/profile/direction.png')} style={{width: 9, height: 10, resizeMode: 'stretch', marginRight: 10}}/>
                            <Text style={{color: "#FF3400", fontSize: 14, marginTop: 5}}>Direction</Text>
                          </View>
                        </View>
                      </View>
                      <View style={{width: '100%', height: '24%', flexDirection: 'row', borderRadius: 10, backgroundColor: '#e8e8e8', padding: 10}}>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                          <Text style={{fontWeight:'bold', color: '#000000', fontSize: 17}}>10 000L</Text>
                          <Text style={{color: '#707070', fontSize: 13}}>Total</Text>
                        </View>
                        <View style={{height: '100%', width: 1, backgroundColor: '#ffffff'}}/>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                          <Text style={{color: '#707070', fontSize: 13}}>Compartment</Text>
                          <Text style={{color: '#707070', fontSize: 13}}>01</Text>
                          <Text style={{fontWeight:'bold', color: '#FF3400', fontSize: 17}}>25 000L</Text>
                        </View>
                        <View style={{height: '100%', width: 1, backgroundColor: '#ffffff'}}/>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                          <Text style={{color: '#707070', fontSize: 13}}>Compartment</Text>
                          <Text style={{color: '#707070', fontSize: 13}}>02</Text>
                          <Text style={{fontWeight:'bold', color: '#FF3400', fontSize: 17}}>25 000L</Text>
                        </View>
                      </View>
                      <View style={{width: '100%', height: "60%"}}>
                        <View style={{width: '100%', height: '20%', marginTop:15, justifyContent: 'space-between'}}>
                          <View style={{flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                              <Image source={require('../../asset/icon/home/yellow.png')} style={{width: 22, height: 22, resizeMode: 'stretch'}}/>
                              <Text style={{color: '#000000', fontSize: 18, marginLeft: 15}}>Diesel</Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-end'}}>
                              <View style={{height: this.state.expand ? 78: 39 ,width: "100%",fontSize: 19, justifyContent: 'center', alignItems: 'center',elevation: 1, borderColor: '#00000028', borderRadius: 6, borderColor: '#00000028', borderWidth: 0.2}}>
                                <View style={{width: '100%',height: '50%', justifyContent: 'space-between', alignItems: 'center', padding: 8, flexDirection: 'row'}}>
                                  <TouchableOpacity onPress={() => this.show_expand()}>
                                    <Text style={{color: '#6A6A6A', fontSize: 13}}>{this.state.compartment1}</Text>
                                  </TouchableOpacity>
                                  <Image source={require('../../asset/icon/booking/showmore.png')} style={{width: 11, height: 11}}/>
                                </View>
                                {this.state.show_option}
                              </View>
                            </View>
                          </View>
                        </View>
                        <View style={{width: '100%', height: '20%', marginTop:15, justifyContent: 'space-between'}}>
                          <View style={{flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                              <Image source={require('../../asset/icon/home/red.png')} style={{width: 22, height: 22, resizeMode: 'stretch'}}/>
                              <Text style={{color: '#000000', fontSize: 18, marginLeft: 15}}>Diesel</Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-end'}}>
                              <View style={{height: this.state.expand ? 78: 39 ,width: "100%",fontSize: 19, justifyContent: 'center', alignItems: 'center',elevation: 1, borderColor: '#00000028', borderRadius: 6, borderColor: '#00000028', borderWidth: 0.2}}>
                                <View style={{width: '100%',height: '50%', justifyContent: 'space-between', alignItems: 'center', padding: 8, flexDirection: 'row'}}>
                                  <TouchableOpacity onPress={() => this.show_expand()}>
                                    <Text style={{color: '#6A6A6A', fontSize: 13}}>{this.state.compartment1}</Text>
                                  </TouchableOpacity>
                                  <Image source={require('../../asset/icon/booking/showmore.png')} style={{width: 11, height: 11}}/>
                                </View>
                                {this.state.show_option}
                              </View>
                            </View>
                          </View>
                        </View>
                        <View style={{width: '100%', height: '20%', marginTop:15, justifyContent: 'space-between'}}>
                          <View style={{flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                              <Image source={require('../../asset/icon/home/blue.png')} style={{width: 22, height: 22, resizeMode: 'stretch'}}/>
                              <Text style={{color: '#000000', fontSize: 18, marginLeft: 15}}>Diesel</Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-end'}}>
                              <View style={{height: this.state.expand ? 78: 39 ,width: "100%",fontSize: 19, justifyContent: 'center', alignItems: 'center',elevation: 1, borderColor: '#00000028', borderRadius: 6, borderColor: '#00000028', borderWidth: 0.2}}>
                                <View style={{width: '100%',height: '50%', justifyContent: 'space-between', alignItems: 'center', padding: 8, flexDirection: 'row'}}>
                                  <TouchableOpacity onPress={() => this.show_expand()}>
                                    <Text style={{color: '#6A6A6A', fontSize: 13}}>{this.state.compartment1}</Text>
                                  </TouchableOpacity>
                                  <Image source={require('../../asset/icon/booking/showmore.png')} style={{width: 11, height: 11}}/>
                                </View>
                                {this.state.show_option}
                              </View>
                            </View>
                          </View>
                        </View>
                        <View style={{width: '100%', height: '20%', marginTop:15, justifyContent: 'space-between'}}>
                          <View style={{flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                              <Image source={require('../../asset/icon/home/green.png')} style={{width: 22, height: 22, resizeMode: 'stretch'}}/>
                              <Text style={{color: '#000000', fontSize: 18, marginLeft: 15}}>Diesel</Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-end'}}>
                              <View style={{height: this.state.expand ? 78: 39 ,width: "100%",fontSize: 19, justifyContent: 'center', alignItems: 'center',elevation: 1, borderColor: '#00000028', borderRadius: 6, borderColor: '#00000028', borderWidth: 0.2}}>
                                <View style={{width: '100%',height: '50%', justifyContent: 'space-between', alignItems: 'center', padding: 8, flexDirection: 'row'}}>
                                  <TouchableOpacity onPress={() => this.show_expand()}>
                                    <Text style={{color: '#6A6A6A', fontSize: 13}}>{this.state.compartment1}</Text>
                                  </TouchableOpacity>
                                  <Image source={require('../../asset/icon/booking/showmore.png')} style={{width: 11, height: 11}}/>
                                </View>
                                {this.state.show_option}
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>     
                </View>
                <TouchableOpacity 
                  onPress={() => this.props.navigation.navigate('ExpectedDAteScreen')}
                  style={{width: '100%', height: '7%',marginTop: Platform.OS === "ios"? 50 : 60, borderRadius: 22, backgroundColor: '#e8e8e8', alignItems: 'center', justifyContent: 'center'}}>
                  <Text style={{color: '#000000', fontSize: 15}}>Expected Date</Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%'
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  },
  
})