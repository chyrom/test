import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, SafeAreaView, FlatList,Modal, ImageBackground,Image,TouchableOpacity, ScrollView, TextInput, YellowBox, } from 'react-native';

export default class ExpectedDate extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
            <SafeAreaView>
              <View style={{paddingLeft: 10, paddingRight: 10, width: '100%', height: '100%'}}>
              <View style={{marginTop: Platform.OS === "ios" ? 10 : 30, width: "100%", alignItems: 'center',height: "5%",flexDirection: 'row'}}>
                <TouchableOpacity 
                style={{width: '10%', justifyContent: 'center', height: '100%'}}
                onPress={() => this.props.navigation.goBack()}> 
                <Image source={require('../../asset/icon/profile/backbtn.png')} 
                style={{width: 10, height: 14,resizeMode: 'stretch'}}/>
                </TouchableOpacity>                
                <View style={{height: '100%', width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={styles.title}>Truck Suggestion</Text>
                </View>
              </View>
                <View style={{marginTop: Platform.OS === "ios" ? 20 : 20, padding: 40,width: '100%', height: Platform.OS === "ios" ? '82%' : '77%', borderRadius: 20, backgroundColor: '#ffffff'}}>
                  <View style={{height: '30%', width: '100%'}}>
                    <Text style={{color: '#000000', fontSize: 18}}>Expected Date</Text>
                    <View style={{flexDirection: 'row', width: '100%', height: 95, marginTop: 15,justifyContent:'space-between'}}>
                      <View style={{alignItems: 'center', borderBottomLeftRadius: 18, borderTopLeftRadius: 18, justifyContent: 'center', width: "49.5%", height: '100%',backgroundColor: '#e8e8e8'}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#000000'}}>25</Text>
                        <Text style={{marginTop: 10}}>March</Text>
                      </View>
                      <View style={{alignItems: 'center', borderTopRightRadius: 18, borderBottomRightRadius: 18,justifyContent: 'center', width: "49.5%", height: '100%', backgroundColor: '#e8e8e8'}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#000000'}}>3 : 40 </Text>
                        <Text style={{marginTop: 10}}>PM</Text>
                      </View>
                    </View> 
                  </View>
                  <View style={{height: '30%', width: '100%', marginTop: 30}}>
                    <Text style={{color: '#000000', fontSize: 18}}>Note</Text>
                    <View style={{width: '100%', height: 130, padding: 15, marginTop: 15, borderRadius: 14, borderWidth: 1, borderColor: '#00000028'}}>
                      <TextInput 
                      placeholder = "Write your note here..."
                      />
                    </View> 
                  </View>
                </View>
                <TouchableOpacity 
                  onPress={() => this.props.navigation.navigate('BookingCompleteScreen')}
                  style={{width: '100%', height: '7%',marginTop: Platform.OS === "ios"? 20 : 10, borderRadius: 22, backgroundColor: '#e8e8e8', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: '#000000', fontSize: 15}}>Start Booking</Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%'
  },
  title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
  },
  
})