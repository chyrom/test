import React, { Component } from "react";
import { Platform,StatusBar,StyleSheet, Text, Alert,View,TextInput,Image,TouchableOpacity, ScrollView,KeyboardAvoidingView} from "react-native";


export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      authorize: '503d0767ac50500bca1e0b840afe03eea9726230142a70a9179a3e11d71a9a9b'
    };
  }

  onChangeEmail = (email) =>{
    this.setState({
      email: email,
    })
  }
  onChangePasword = (password) =>{
    this.setState({
      password: password,
    })
  }
  _reqLog = () =>{
    if (this.state.email == ""  || this.state.password == "") {
      // Alert.alert("Failed","Email or Password can't be empty.!");
      this.props.navigation.navigate('BottomTap')
      return;
    }
    fetch('http://bvm-admin-dev.codingate.com/api/customer/auth/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorize': this.state.authorize,
        'language':'en'
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      }),
    }).then((response) => response.json())
    .then((responseJson)=>{
      if(responseJson.success == true) {
        console.log(responseJson.data);
        // this.props.navigation.navigate('BottomTap',{
        //   authorize: this.state.authorize,
        //   auth: responseJson.data.access_token
        // })
      }
      else if (responseJson.success == false){
        Alert.alert("Login failed",responseJson.message);

      }
    });
  }
  render() {
    return (
      <View style={styles.bgContainer}>
        <StatusBar translucent backgroundColor="transparent" />
        <View style={{flex:1.5,justifyContent:'center'}}>
          <Image source={require('../asset/icon/login/containerlogo.png')}
            style={{width:'100%',height:'100%',resizeMode:'stretch'}}>
          </Image>
          <Image source={require('../asset/icon/login/logo.png')}
          style={{width:110,height:110,alignSelf:'center',resizeMode:'stretch',position:'absolute'}}/>
        </View>
        <View style={{flex:2,paddingLeft:18,paddingRight:18}}>
          <View style={{flex:1}}>
            <View style={{marginTop:50,backgroundColor:'#fff',paddingLeft:26,paddingRight:26,alignItems:'center',borderRadius:26,flexDirection:'row'}}>
              <Image source={require('../asset/icon/login/phone.png')}
              style={{width: Platform.OS === "ios" ? 16 : 20,height: Platform.OS === "ios" ? 16 : 20,resizeMode:'stretch'}}/>
              <TextInput
                placeholder="Email"
                placeholderTextColor="#00000080"
                style={{fontSize:18,padding:12, width:'100%'}}
                onChangeText = { email =>this.onChangeEmail(email)}
                required = 'required'
                requiredMessage ='Please enter email'
                keyboardType = 'email-address'  
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.passwordField.focus(); }}
                />
            </View>
            <View
              style={{marginTop:13,backgroundColor:'#fff',paddingLeft:26,paddingRight:26,borderRadius:26,flexDirection:'row',alignItems:'center'}}>
                <Image source={require('../asset/icon/login/password.png')}
                  style={{width: Platform.OS === "ios" ? 16 : 20,height: Platform.OS === "ios"? 16 : 20,resizeMode:'stretch'}}/>
                <TextInput
                  placeholder="Password"
                  placeholderTextColor="#00000080"
                  style={{fontSize:18,padding:12, width:'100%', width: '100%'}}
                  onChangeText = { password =>this.onChangePasword(password)}
                  required = 'required'
                  requiredMessage ='Please enter email'
                  keyboardType = 'default'
                  // secureTextEntry = {true}
                  ref={(input) => { this.passwordField = input; }}
                />
              </View>
            </View>
            <View style={{flex:1}}>
              <View style={{justifyContent:'center',alignItems:'flex-start'}}>
              <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => this._reqLog()}
                    // onPress={() => this.props.navigation.navigate('BottomTap')}
                    style={{padding:14,backgroundColor:'#f0bc45',borderRadius:25,width:'100%'}}>
                    <Text style={{color:'#000000',fontWeight:'bold',fontSize:14,alignSelf:'center'}}>LOGIN</Text>
                  </TouchableOpacity>
              </View>
              <TouchableOpacity style={{flex:1,justifyContent:'flex-end',alignItems:'center',paddingBottom: Platform.OS === "ios" ? 40 : 30 }}>
                <Text style={{fontSize:15,color:'#00000080'}}>Forgot Password</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    );
  }
}   

const styles = StyleSheet.create({
bgContainer:{
  flex:1,
  backgroundColor:'#e3e3e3'
}

})
