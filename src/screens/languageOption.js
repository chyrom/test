import React, {Component} from 'react';
import {Platform, StyleSheet,Image,Text, StatusBar, View,SafeAreaView,TouchableOpacity} from 'react-native';
import SplashScreen from 'react-native-splash-screen'

export default class LanguageOption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isKh: false,
      isEn: true
    };
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar translucent backgroundColor="transparent" />
        <View style={styles.logoContainer}>
          <Image source={require('../asset/icon/langaugeOption/bvm.png')} style={styles.logo}/>
        </View>
        <View style={styles.backgroundImgContainer}>
          <Image source={require('../asset/icon/langaugeOption/background.png')} style={styles.backgroundImg}/>
        </View>
        <View style={styles.languageOptionContainer}>
          <View style={[styles.textContainer,{borderTopRightRadius:4,borderTopLeftRadius:4}]}>
            <Text style={styles.text}>Select Language</Text>
          </View>
          <View style={styles.languageContainer}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.setState({isEn: false, isKh: true})}>
                <Text style={{ color: this.state.isKh ? "#017C5F" : "#000000", fontSize: 16}}>ភាសាខ្មែរ</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.roler}></View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}> 
            <TouchableOpacity onPress={() => this.setState({isEn: true, isKh: false})}>
              <Text style={{ color: this.state.isEn ? "#017C5F" : "#000000", fontSize: 16}}>English</Text>
            </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity 
            activeOpacity={0.5}
            onPress={() => this.props.navigation.navigate('LoginScreen')}
            style={[styles.textContainer,{borderBottomRightRadius:4,borderBottomLeftRadius:4}]}>
            <Text style={[styles.text,{fontWeight:'bold'}]}>OK</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',  
  },
  backgroundImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch'
  },
  logo: {
    width: 125,
    height: 130,
    resizeMode: 'stretch',
    marginTop: -35
  },
  logoContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: -35
  },
  backgroundImgContainer: {
    flex: 1
  },
  languageOptionContainer: {
    borderRadius: 8,
    width: '95%',
    height: 210,
    position:'absolute',
    backgroundColor:'yellow',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    elevation:20,
    shadowColor: '#00000029',
    borderColor:'#00000029',
    borderWidth:0.1,
  },
  roler: {
    height: 0.4,
    backgroundColor: '#707070',
    width: '60%'
  },
  languageContainer:{
    backgroundColor: '#FFFFFF',
    flex:4,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textContainer:{
    backgroundColor: '#F0BC45',
    flex:1.5,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text:{
    color: '#000000',
    fontSize: 15
  },

})
