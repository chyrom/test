import React, {Component} from 'react';
import {Platform, SafeAreaView, StyleSheet, Text, View, ImageBackground,Image,TouchableOpacity} from 'react-native';

export default class EditProfile extends Component {
    render() {
      return (
      <ImageBackground source={require('../../asset/icon/background.png')} style={styles.backgroundImg}>
        <SafeAreaView>
          <View style={{flex: 1}}>
            <View style={{width: '100%',height: "10%"}}>
              <View 
              style={{width: "100%", height: 20, marginLeft: 17, marginTop: Platform.OS === "ios" ? 14: 38, justifyContent: 'center'}}> 
                <TouchableOpacity 
                  style={{width: 40, height: "100%"}}
                  onPress={() => this.props.navigation.goBack()}>
                  <Image source={require('../../asset/icon/profile/backbtn.png')} 
                  style={{width: 10, height: 17,resizeMode: 'stretch'}}/>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.profileContainer}>
              <ImageBackground source={require('../../asset/icon/profile/style.png')} 
                style={{width: '100%',height:'72%',justifyContent: 'center', alignItems: 'center',resizeMode: 'stretch'}}>
                <View style={styles.profile}>
                  <Image source={require('../../asset/icon/profile/profile.png')} 
                  style={styles.photo}/>
                  <TouchableOpacity style={styles.cameraContainer}>
                  <Image source={require('../../asset/icon/profile/camera.png')}
                  style={styles.camera}/>
                  </TouchableOpacity>
                </View>
              </ImageBackground>
              <View style={{width: '100%',alignItems: 'center',marginTop: -15}}>
                <Text style={styles.name}>Customer Name</Text>
              </View>  
            </View>
            <View style={{width: '100%',height: 500,paddingLeft: 6,paddingRight: 6,marginTop: 18}}>
              <View style={{flexDirection:'row',padding: 8}}>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                  <Image source={require('../../asset/icon/profile/account.png')} 
                  style={{width:17, height:17}} />
                </View>
                <View style={{alignItems:'center',marginLeft:20}}>
                  <Text style={{fontSize:18,color:'#ffffff',alignSelf:'flex-start'}}>Som Davin</Text>
                  <Text style={{alignSelf:'flex-start',color:'#ffffff',marginTop: 8}}>UserName</Text>
                </View>
                <View style={{alignItems:'flex-end', justifyContent:'center',marginLeft:'auto'}}>
                  <Image source={require('../../asset/icon/profile/pen.png')}
                  style={{alignSelf:'flex-end',marginLeft:'auto',width:17,height:17}}/>
                </View>
              </View>
              <View style={{flexDirection:'row',padding:7,marginTop:8}}>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                  <Image source={require('../../asset/icon/profile/phone.png')}
                  style={{width:17, height:17}} />
                </View>
                <View style={{alignItems:'flex-start',marginLeft:20}}>
                  <Text style={{fontSize:18,color:'#ffffff',alignSelf:'flex-start'}}>+885 12 223 332</Text>
                  <Text style={{alignSelf:'flex-start',color:'#ffffff',marginTop: 8}}>Phone Number</Text>
                </View>
                <View style={{alignItems:'flex-end', justifyContent:'center',marginLeft:'auto'}}>
                  <Image source={require('../../asset/icon/profile/pen.png')}
                  style={{alignSelf:'flex-end',marginLeft:'auto',width:16,height:16, resizeMode: 'stretch'}}/>
                </View>
              </View>

              <View style={{flexDirection:'row',padding:7,marginTop:8}}>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                  <Image source={require('../../asset/icon/profile/mail.png')} 
                  style={{width:16,height:16,resizeMode:'stretch'}}/>
                </View>
                <View style={{alignItems:'flex-start',marginLeft:20}}>
                  <Text style={{fontSize:18,color:'#ffffff',alignSelf:'flex-start'}}>sovandavin@gmail.com</Text>
                  <Text style={{alignSelf:'flex-start',color:'#ffffff',marginTop: 8}}>Gmail address</Text>
                </View>
                <View style={{alignItems:'flex-end', justifyContent:'center',marginLeft:'auto'}}>
                  <TouchableOpacity>
                    <Image source={require('../../asset/icon/profile/pen.png')}
                    style={{alignSelf:'flex-end',marginLeft:'auto',width:16,height:16, resizeMode: 'stretch'}}/>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={{flexDirection:'row',padding:7}}>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                  <Image source={require('../../asset/icon/profile/password.png')} 
                  style={{width:20,height:20}}/>
                </View>
                <View style={{alignItems:'center',justifyContent:'center',marginLeft:10}}>
                  <Text style={{fontSize: 44,color:'#ffffff',fontWeight:'bold'}}>. . . . . . . . .</Text>
                  <Text style={{alignSelf:'flex-start',color:'#ffffff',marginTop: 8}}>Password</Text>
                </View>
                <View style={{alignItems:'flex-end', justifyContent:'center',marginLeft:'auto'}}>
                  <Image source={require('../../asset/icon/profile/pen.png')}
                  style={{alignSelf:'flex-end',marginLeft:'auto',width:17,height:17}}/>
                </View>
              </View>
            </View>
          </View>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImg: {
    width: '100%',
    height: '100%'
  },
  cameraContainer:{
    width:40,
    height:40,
    borderRadius:20,
    position:'absolute',
    bottom:0,
    right:0,
  },
  photo:{
    width:130,
    height:130,
    borderRadius:65,
  },
  profile:{
    width:130,
    height:130,
    borderRadius:65,
    justifyContent:'center',
    alignItems: 'center'
  },
  name:{
    color:'#ffffff',
    fontSize: 20
  },
  camera:{
    width:40,
    height:40,
    borderRadius:20,
    bottom:0,
  },
  profileContainer:{
    height: 270,
    width: '100%',
    alignItems:'center',
    justifyContent: 'center',
  },
  
})