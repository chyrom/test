import React, {Component} from 'react';
import {Platform, StyleSheet, Switch, Text, View,SafeAreaView,Image,ScrollView ,TouchableOpacity,ImageBackground} from 'react-native';

export default class Profile extends Component {
    render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={require('../../asset/icon/background.png')} style={styles.imageBackground}>
          <ScrollView style={{width: '100%', height: '100%'}}>
            <SafeAreaView>
              <View style={{width: '100%', height: '100%',marginBottom: 100}}>
                <View style={styles.profileContainer}>
                  <ImageBackground source={require('../../asset/icon/profile/style.png')} 
                    style={{width: '100%',height:'72%',justifyContent: 'center', alignItems: 'center',resizeMode: 'stretch'}}>
                    <View style={styles.profile}>
                      <Image source={require('../../asset/icon/profile/profile.png')} 
                      style={{width:130,height:130,borderRadius:65}}/>
                      <TouchableOpacity style={styles.cameraContainer}>
                      <Image source={require('../../asset/icon/profile/camera.png')}
                      style={styles.camera}/>
                      </TouchableOpacity>
                    </View>
                  </ImageBackground>
                  <View style={{width: '100%',alignItems: 'center',marginTop: -15}}>
                    <Text style={styles.name}>Customer Name</Text>
                  </View>  
                </View>
              <View style={styles.info}> 
                  <View style={styles.touchableOpacityContainer}>
                    <TouchableOpacity style={styles.touchableOpacity}      
                    onPress={() => this.props.navigation.navigate('EditProfileScreen')}>
                      <Image source={require('../../asset/icon/profile/account.png')} style={{width:18,height:18,resizeMode:'contain'}}/>
                      <Text style={styles.text}>Edit Profile</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.touchableOpacityContainer}>
                    <View style={styles.touchableOpacity}>
                      <Image source={require('../../asset/icon/profile/notification.png')}  style={{width:18,height:18,resizeMode:'contain'}}/>
                      <Text style={styles.text}>Notification</Text>
                    </View>
                    <View style={{flex:1,alignItems: 'flex-end'}}>
                      <Switch 
                        style={{transform: Platform.OS === 'android' ? [{scaleX: 1.2}, {scaleY: 1.15}] : [{scaleX: 0.8}, {scaleY: 0.75}]}} 
                        trackColor = {{false: '#F0BC45', true: '#F0BC45'}}
                        thumbColor = "#ffffff"
                        />
                    </View>
                  </View>
                  <View style={styles.touchableOpacityContainer}>
                  <TouchableOpacity style={styles.touchableOpacity}>
                      <Image source={require('../../asset/icon/profile/signout.png')}  style={{width:18,height:18,resizeMode:'contain'}}/>
                      <Text style={styles.text}>Log Out</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.touchableOpacityContainer}>
                    <View style={styles.touchableOpacity}>
                      <Image source={require('../../asset/icon/profile/language.png')} style={{width:18,height:18,resizeMode:'contain'}}/>
                      <Text style={[styles.text]}>Language</Text>
                    </View>
                    <View style={{flex:1,alignItems:'flex-end'}}>
                      <Text style={{color:'#ffffff', fontSize: 16}}>English</Text>
                    </View>
                  </View>
                </View>
                <View style={{paddingLeft: 10,paddingRight: 10}}>
                  <Text style={{color: '#ffffff',fontSize: 20}}>Choose Location</Text>
                  <View style={{backgroundColor: '#ffffff',borderRadius: 12,width:'100%',height: 100,padding: 15,marginTop: 30}}>
                    <View style={{flexDirection: 'row',alignItems:'center'}}>
                      <Image source={require('../../asset/icon/profile/direction.png')} style={{width:15,height:20,resizeMode: 'stretch'}}/>
                      <Text style={{marginLeft: 18,fontSize: 17,color: '#000000',fontWeight: 'bold'}}>Kompong Spue</Text>
                    </View>
                    <View style={{marginTop: 20}}>
                      <Text style={{marginLeft: 32,fontSize: 16,color:'#707070'}}># NR4, Krong Chbar Mon</Text>
                    </View>
                  </View>
                  <View style={{backgroundColor: '#ffffff',borderRadius: 12,width:'100%',height: 100,padding: 15,marginTop: 15}}>
                    <View style={{flexDirection: 'row',alignItems:'center'}}>
                      <Image source={require('../../asset/icon/profile/direction.png')} style={{width:15,height:20,resizeMode: 'stretch'}}/>
                      <Text style={{marginLeft: 18,fontSize: 17,color: '#000000',fontWeight: 'bold'}}>Phnom Penh</Text>
                    </View>
                    <View style={{marginTop: 20}}>
                      <Text style={{marginLeft: 32,fontSize: 16,color:'#707070'}}># St 132, SangKat Tuek Thla</Text>
                    </View>
                  </View>
                  <View style={{backgroundColor: '#ffffff',borderRadius: 12,width:'100%',height: 100,padding: 15,marginTop: 15}}>
                    <View style={{flexDirection: 'row',alignItems:'center'}}>
                      <Image source={require('../../asset/icon/profile/direction.png')} style={{width:15,height:20,resizeMode: 'stretch'}}/>
                      <Text style={{marginLeft: 18,fontSize: 17,color: '#000000',fontWeight: 'bold'}}>Kompong Spue</Text>
                    </View>
                    <View style={{marginTop: 20}}>
                      <Text style={{marginLeft: 32,fontSize: 16,color:'#707070'}}># NR4, Krong Chbar Mon</Text>
                    </View>
                  </View>
                  <View style={{backgroundColor: '#ffffff',borderRadius: 12,width:'100%',height: 100,padding: 15,marginTop: 15}}>
                    <View style={{flexDirection: 'row',alignItems:'center'}}>
                      <Image source={require('../../asset/icon/profile/direction.png')} style={{width:15,height:20,resizeMode: 'stretch'}}/>
                      <Text style={{marginLeft: 18,fontSize: 17,color: '#000000',fontWeight: 'bold'}}>Phnom Penh</Text>
                    </View>
                    <View style={{marginTop: 20}}>
                      <Text style={{marginLeft: 32,fontSize: 16,color:'#707070'}}># St 132, SangKat Tuek Thla</Text>
                    </View>
                  </View>
                </View>
              </View>
            </SafeAreaView>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  imageBackground: {
    width: '100%',
    height: '100%'
  },
  profileContainer:{
    height: 270,
    width: '100%',
    alignItems:'center',
    justifyContent: 'center'
  },
  info:{
    height: 264,
    width: '100%'
  },
  profile:{
    width:130,
    height:130,
    borderRadius:65,
  },
  imgbg:{
   width:'90%',
   height:'90%',
   justifyContent:'center',
   alignSelf:'center'
  },
  name:{
    color:'#ffffff',
    fontSize: 20
  },
  cameraContainer:{
    width:40,
    height:40,
    borderRadius:20,
    position:'absolute',
    bottom:0,
    right:0
  },
  camera:{
    width:40,
    height:40,
    borderRadius:20,
    bottom:0,
  },
  icon:{
    width:8,
    height:8
},
  touchableOpacity:{
    flexDirection:'row',
    alignItems:'center'
  },
  touchableOpacityContainer:{
    paddingTop:18,
    paddingBottom:18,
    paddingLeft:18,
    paddingRight:18,
    flexDirection:'row',
  },
  text:{
    marginLeft:20,
    color:'#ffffff',
    fontSize:16
  }


})

