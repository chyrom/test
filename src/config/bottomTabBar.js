import { createBottomTabNavigator,createAppContainer, createStackNavigator } from 'react-navigation';
import HomeMenu from './../screens/homeScreens/homeMenu';
import Booking from './../screens/bookings/booking';
import Profile from './../screens/profiles/profile';
import Notification from './../screens/notifications/notification';
import OrderHistory from './../screens/orderHistorys/orderHistory';
import TabBarComponent from './tabBarComponent';

const BottomNavigatorScreen = createBottomTabNavigator({
  HomeScreen: HomeMenu,
  BookingScreen: Booking,
  OrderHistoryScreen: OrderHistory ,
  NotificationScreen: Notification,
  ProfileScreen: Profile 
},{
  tabBarComponent: TabBarComponent
});

export default createAppContainer(BottomNavigatorScreen);
