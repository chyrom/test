import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, ScrollView,TouchableOpacity,Image } from 'react-native';
import { createStackNavigator,createAppContainer, createSwitchNavigator } from 'react-navigation';
import LanguageOption from './../screens/languageOption';
import LoginForm from './../screens/loginForm';
import BottomNavigatorScreen from './bottomTabBar';
import Profile from './../screens/profiles/profile';
import EditProfile from './../screens/profiles/editProfile';
import Notification from './../screens/notifications/notification'
import NotificationDetail from './../screens/notifications/notificationDetail';
import ComingUp from './../screens/homeScreens/comingUp';
import HomeMenu from './../screens/homeScreens/homeMenu';
import OrderHistory from './../screens/orderHistorys/orderHistory';
import OrderHistoryDetail from './../screens/orderHistorys/orderHistoryDetail';
import SendIssue from './../screens/orderHistorys/sendIssue';
import CompletedIssue from './../screens/orderHistorys/completedIssue';
import Pending from './../screens/homeScreens/pending';
import Completed from './../screens/homeScreens/completed';
import Feedback from './../screens/homeScreens/feedback';
import TruckSuggestion from './../screens/bookings/truckSuggestion';
import TruckSuggestionDetail from './../screens/bookings/truckSuggestionDetail';
import ExpectedDate from './../screens/bookings/ExpectedDate';
import BookingComplete from './../screens/bookings/bookingComplete';

const AuthenticationScreen = createStackNavigator({
  LanguageOptionScreen: LanguageOption,
  LoginScreen: LoginForm
}, {
  headerMode: 'none'
});

const HomeStackScreen = createStackNavigator({
  BottomTap: BottomNavigatorScreen,
  ProfileScreen: Profile,
  EditProfileScreen: EditProfile,
  NotificationScreen: Notification,
  NotificationDetailScreen: NotificationDetail,
  ComingUpScreen: ComingUp,
  HomeMenuScreen: HomeMenu,
  OrderHistoryScreen: OrderHistory,
  OrderHistoryDetailScreen: OrderHistoryDetail,
  SendIssueScreen: SendIssue,
  CompletedIssueScreen: CompletedIssue,
  PendingScreenDetail: Pending,
  CompletedScreen: Completed,
  FeedbackScreen: Feedback,
  TruckSuggestionScreen: TruckSuggestion,
  TruckSuggestionDetailScreen: TruckSuggestionDetail,
  ExpectedDAteScreen: ExpectedDate,
  BookingCompleteScreen: BookingComplete
}, {
  headerMode: 'none'
});

const SwitchNavigatorScreen = createSwitchNavigator({
  AuthenticationScreens: AuthenticationScreen,
  HomeStackScreens: HomeStackScreen
});

export default createAppContainer(SwitchNavigatorScreen);
