import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import {withNavigation} from 'react-navigation';

class TabBarComponentScreen extends Component {

  renderTab = (screen = 'HomeScreen') => {
    if(this.props.navigation.state.routes[this.props.navigation.state.index].routeName !== screen){
      this.props.navigation.navigate(screen);
    }
  };

  render() {
    return (
      <View style={styles.bottomTabBar}>
        <View style={{flexDirection:'row',justifyContent: 'space-around', alignItems: 'center'}}> 
          <TouchableOpacity 
            onPress={() => this.renderTab('HomeScreen')} style={{alignSelf:"center"}}>
            <Image source= {this.props.navigation.state.index === 0 ? require('../asset/icon/bottomTabBar/activeHome.png') : require('../asset/icon/bottomTabBar/inactiveHome.png')} 
            style={{width:23,height:23,resizeMode:'stretch'}}/>
          </TouchableOpacity>
          <TouchableOpacity style={{alignSelf:"center"}}
            onPress={() => this.renderTab('BookingScreen')}>
            <Image source={this.props.navigation.state.index === 1 ? require('../asset/icon/bottomTabBar/activeBooking.png') : require('../asset/icon/bottomTabBar/inactivebooking.png')} 
            style={{width:23,height:23,resizeMode:'stretch'}}/>
          </TouchableOpacity>
          <TouchableOpacity style={{alignSelf:"center"}}  
            onPress={() => this.renderTab('OrderHistoryScreen')} >
            <Image source={this.props.navigation.state.index === 2 ? require('../asset/icon/bottomTabBar/activeOH.png') : require('../asset/icon/bottomTabBar/inactiveOH.png')} 
            style={{width:23,height:23,resizeMode:'stretch'}}/>
          </TouchableOpacity>
          <TouchableOpacity style={{alignSelf:"center"}}
            onPress={() => this.renderTab('NotificationScreen')} >
            <Image source={this.props.navigation.state.index === 3 ? require('../asset/icon/bottomTabBar/activenotification.png') : require('../asset/icon/bottomTabBar/inactivenotification.png')} 
            style={{width:26,height:26,resizeMode:'stretch'}}/>
          </TouchableOpacity>
          <TouchableOpacity style={{alignSelf:"center"}}
            onPress={() => this.renderTab('ProfileScreen')} >
            <Image source={this.props.navigation.state.index === 4 ? require('../asset/icon/bottomTabBar/activeprofile.png') : require('../asset/icon/bottomTabBar/inactiveprofile.png')} 
            style={{width:26,height:26,resizeMode:'stretch'}}/>
          </TouchableOpacity>
        </View>  
      </View>
    );
  }
};

const styles = StyleSheet.create({
    bottomTabBar: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        paddingTop: Platform.OS === "ios" ? 16 : 15,
        paddingBottom: Platform.OS === "ios" ? 16 : 15,
        backgroundColor: "#ffffff",
        borderRadius: 30,
        marginLeft: Platform.OS === "ios" ? 10 : 10,
        marginRight: Platform.OS === "ios" ? 10 : 10,
        marginBottom: Platform.OS === "ios" ? 18 : 12,
        borderColor: '#0000003F',
        shadowOpacity: 1, 
        shadowOffset: { width: 1, height: 2 },
        shadowRadius: 2,
        elevation: 4,
        shadowColor: 'rgba(0, 0, 0, 0.26)'
    }
});

export default withNavigation(TabBarComponentScreen);
